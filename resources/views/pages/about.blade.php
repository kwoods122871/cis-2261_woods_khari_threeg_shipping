@extends('layouts.app')
    @section('content')

    <div class="about-container">
        <h1>{{$title}}</h1>
        <p>A small family operated Freight Forwarding Company.</p>
        <p>We ship packages from Miami to Nassau. We also delivery to your door step free of charge !</p>
        <hr>
        <h1>Customer Features</h1>
        <ul>
            <li>Upload Invoice</li>
            <li>Upload Package Tracking #</li>
            <li>View Package Details</li>
            <li>Delete Package Content</li>
        </ul>

        <h1>Admin Features</h1>
        <ul>
            <li>Receive Packages</li>
            <li>View Packages in Inventory</li>
            <li>Perform CRUD operations on Packages in Inventory</li>
            <li>Create and manage invoices for each item inventory</li>
            <li>Create and manage customer and administrative accounts</li>
        </ul>
    </div>

        
@endsection