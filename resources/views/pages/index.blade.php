@extends('layouts.app')
    @section('content')
        
        <div class="container">
            <div class="custom-container content-container">
                <div class="overlay">
                    <h1>{{$title}}</h1>
                    <h4>Fast, Convinient and Reliable Shipping !</h4>
                </div>
            </div>
        </div>
    @endsection    
